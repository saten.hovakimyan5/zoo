package org.learn.qa4;

import java.time.LocalDate;

public abstract class Fish extends Animal implements Moving {
  protected Fish(String name, LocalDate bDate) {
    super(name, bDate);
  }
}
