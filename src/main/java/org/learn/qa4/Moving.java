package org.learn.qa4;

public interface Moving extends MakeSound {
  default void move() {
    System.out.println(this.doSound());
  }
}
