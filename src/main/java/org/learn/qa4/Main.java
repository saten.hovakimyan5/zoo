package org.learn.qa4;

import java.time.LocalDate;
import java.time.Period;
import java.util.Arrays;
import java.util.Comparator;

public class Main {
  public static void main(String[] args) {
    printCount();

    Animal[] animals = {
        new Cat("Kitten", LocalDate.now()),
        new Duck("Duckling", LocalDate.now()),
        new Duck("Duckling", LocalDate.now().minus(Period.ofDays(5))),
        new HomeFish("Goldy", LocalDate.now())
    };

    printCount();

    for (Animal animal : animals) {
      System.out.println(animal);
    }

    Arrays.sort(animals, (o1, o2) -> {
          if (o2 == o1) {
            return 0;
          }
          if (o1 == null || o2 == null) {
            return 0;
          }
          int nameCompare = o1.getName().compareTo(o2.getName());
          if (nameCompare != 0) {
            return nameCompare;
          }
          return o1.getBDate().compareTo(o2.getBDate());
        });

    for (Animal animal : animals) {
      System.out.println(animal);
      System.out.println(Feeding.feed(animal));
    }

  }

  private static void printCount() {
    System.out.printf("Animal count is: %s%n", Animal.getCount());
  }
}