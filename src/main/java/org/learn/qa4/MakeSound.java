package org.learn.qa4;

public interface MakeSound {
  String doSound();

  static String description() {
    return "description";
  }
}
