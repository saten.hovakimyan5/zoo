package org.learn.qa4;

import java.time.LocalDate;

public class HomeFish extends Fish {
  public HomeFish(String name, LocalDate bDate) {
    super(name, bDate);
  }

  @Override
  public String doSound() {
    return null;
  }
}
