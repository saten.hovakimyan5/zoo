package org.learn.qa4;

import java.time.LocalDate;

public class Cat extends Animal implements MakeSound {
  private static String SOUND = "meow";
  protected Cat(String name, LocalDate bDate) {
    super(FeedingType.CARNIVORES, name, bDate);
    System.out.printf("I'm the %sth animal in the Zoo. %n", Animal.getCount());
  }

  public String doSound() {
    return SOUND;
  }

  public String toString() {
    return super.toString()
        + " It says: " + doSound();
  }

}
