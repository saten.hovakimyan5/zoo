package org.learn.qa4;

import java.time.LocalDate;

public class Duck extends Animal implements MakeSound {
  private static String SOUND = "krya";
  protected Duck(String name, LocalDate bDate) {
    super(name, bDate);
    System.out.printf("I'm the %sth animal in the Zoo. %n", Animal.getCount());
  }

  public String doSound() {
    return SOUND;
  }

  public String toString() {
    return super.toString()
        + " It says: " + doSound();
  }

}
